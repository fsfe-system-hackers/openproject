# OpenProject

This is an installation of [OpenProject](https://www.openproject.org/) for the
FSFE. It is in an early evaluation phase, not connected to our central user
database, and may contain bugs.

If you would like to have access, please contact @max.mehl.
